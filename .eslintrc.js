module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.json',
    ecmaVersion: 2021,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  plugins: [
    '@typescript-eslint',
    'react',
    'react-hooks',
    'jsx-a11y',
    'import',
  ],
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:@next/next/recommended', // Utilize a configuração recomendada do Next.js
  ],
  rules: {
    'quotes': 'off', // Desativa a regra de aspas
    'import/no-unresolved': 'off', // Desativa a regra import/no-unresolved
    'react/react-in-jsx-scope': 'off', // Desativa a regra que exige React em escopo ao usar JSX
    'react/no-unescaped-entities': 'off', // Desativa a regra que impede entidades não escapadas em JSX
    'no-useless-catch': 'off', // Desativa a regra no-useless-catch
    '@typescript-eslint/no-explicit-any': 'off'
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
};
