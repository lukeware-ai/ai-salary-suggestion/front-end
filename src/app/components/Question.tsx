import { useRouter } from "next/router";
import React from "react";
import { GoHomeFill } from "react-icons/go";

interface QuestionProps {
  question: string;
  children: React.ReactNode;
  onNext: () => void;
  onPrevious?: () => void;
  title?: string;
}

const Question: React.FC<QuestionProps> = ({
  title = "Questão",
  question,
  children,
  onNext,
  onPrevious,
}) => {
  const router = useRouter();
  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <GoHomeFill
            className="w-7 h-7 text-gray-700"
            onClick={() => router.push("/")}
          />
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-md">
          <h1 className="text-base sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
            {title}
          </h1>
          <h2 className="text-[17px] sm:text-xl font-light mb-4 text-gray-700 text-justify">
            {question}
          </h2>
          <div className="mb-4">{children}</div>
          <div className="flex flex-col md:flex-row justify-between">
            {onPrevious && (
              <button
                className="bg-gray-300 text-white py-2 px-4 md:px-10 rounded hover:bg-gray-400 uppercase mb-2 md:mb-0 md:mr-2"
                onClick={onPrevious}
              >
                Back
              </button>
            )}
            <button
              className="bg-purple-500 text-white py-2 px-4 md:px-10 rounded hover:bg-purple-700 uppercase mb-2 md:mb-0 md:mr-2"
              onClick={onNext}
            >
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Question;
