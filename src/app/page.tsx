"use client";

import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { FcOk, FcHighPriority } from "react-icons/fc";
import { GoHomeFill } from "react-icons/go";

export default function Page() {
  const router = useRouter();
  const [status, setStatus] = useState<boolean>(false);

  const startSurvey = () => {
    router.push("/question_1");
  };

  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch("/api/check_health");
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();
  }, []);

  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <GoHomeFill
            className="w-7 h-7 text-gray-700"
            onClick={() => router.push("/")}
          />
        </div>
      </div>
      <div className="relative">
        <div className="absolute top-0 right-0 m-4">
          {status === true && <FcOk className="w-5 h-5 text-gray-700" />}
          {status === false && (
            <FcHighPriority className="w-5 h-5 text-gray-700" />
          )}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="p-6 rounded-lg shadow-lg w-full max-w-md bg-white">
          <h1 className="text-base sm:text-2xl font-semibold mb-4 text-gray-700 uppercase">
            Enhancing Salary Estimates with Salary Suggestion AI
          </h1>
          <h2 className="text-[17px] sm:text-xl font-light text-justify mb-4 text-gray-700">
            Hello, my name is Salary Suggestion AI. I am an artificial
            intelligence designed to provide more accurate salary estimates. To
            do this, it is crucial to understand the candidate's profile and
            skills. By answering these questions, I can estimate the average
            salary of a software developer in Brazil more precisely.
          </h2>
          <button
            className="bg-purple-500 text-white py-2 px-10 rounded hover:bg-purple-700 ml-auto float-end uppercase"
            onClick={startSurvey}
          >
            Starting
          </button>
        </div>
      </div>
    </>
  );
}
