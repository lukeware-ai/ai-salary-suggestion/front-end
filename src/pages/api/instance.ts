// pages/api/predict.ts
import { NextApiRequest, NextApiResponse } from "next";

type PredictRequestBody = {
  tempo_experiencia: number;
  faculdade_concluida_0_1: number;
  pos_graduado_0_1: number;
  conhecimento_programacao_0_10: number;
  conheciemento_das_ferramentas_trabalho_0_10: number;
  conhecimento_banco_de_dados_0_10: number;
  conhecimento_back_end_0_10: number;
  conhecimento_front_end_0_10: number;
  conhecimento_devops_0_10: number;
  communicacao_0_10: number;
  dominio_sistemas_operacionais_0_5: number;
  conhecimento_basico_rede_0_5: number;
  lideranca_0_10: number;
  pj_0_1: number;
  salario: number;
};

type PredictResponse = {
  status: string;
};

type PredictErrorResponse = {
  error: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<PredictResponse | PredictErrorResponse>
) {
  if (req.method !== "PUT") {
    return res
      .status(405)
      .json({ status: "error", error: "Method Not Allowed" });
  }

  try {
    const {
      tempo_experiencia,
      faculdade_concluida_0_1,
      pos_graduado_0_1,
      conhecimento_programacao_0_10,
      conheciemento_das_ferramentas_trabalho_0_10,
      conhecimento_banco_de_dados_0_10,
      conhecimento_back_end_0_10,
      conhecimento_front_end_0_10,
      conhecimento_devops_0_10,
      communicacao_0_10,
      dominio_sistemas_operacionais_0_5,
      conhecimento_basico_rede_0_5,
      lideranca_0_10,
      pj_0_1,
      salario,
    } = req.body as PredictRequestBody;

    const data_request = JSON.stringify({
      tempo_experiencia,
      faculdade_concluida_0_1,
      pos_graduado_0_1,
      conhecimento_programacao_0_10,
      conheciemento_das_ferramentas_trabalho_0_10,
      conhecimento_banco_de_dados_0_10,
      conhecimento_back_end_0_10,
      conhecimento_front_end_0_10,
      conhecimento_devops_0_10,
      communicacao_0_10,
      dominio_sistemas_operacionais_0_5,
      conhecimento_basico_rede_0_5,
      lideranca_0_10,
      pj_0_1,
      salario,
    });

    console.log(data_request);

    const response = await fetch(
      "http://192.168.15.66:8881/calc-salary/api/v1/instance",
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: data_request,
      }
    );

    if (!response.ok) {
      throw new Error("Failed to fetch data from prediction service");
    }

    const response_2 = await fetch(
      "http://192.168.15.66:8881/calc-salary/api/v1/model",
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (!response_2.ok) {
      throw new Error("Failed to fetch data from prediction service");
    }

    const data = await response_2.json();
    res.status(200).json(data);
  } catch (error: any) {
    res.status(500).json({ error: error.message });
  }
}
