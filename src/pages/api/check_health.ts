import type { NextApiRequest, NextApiResponse } from 'next';

type Data = {
  status: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  try {
    const response = await fetch('http://192.168.15.66:8881/calc-salary/api/v1/health');
    const data = await response.json();

    if (response.ok) {
      res.status(200).json({ status: data.status });
    } else {
      res.status(response.status).json({ status: 'error' });
    }
  } catch (error) {
    res.status(500).json({ status: 'error' });
  }
}