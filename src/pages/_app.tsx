import React from 'react';
import { AppProps } from 'next/app';
import { QuestionProvider } from '@/contexts/QuestionContext';
import "@/app/globals.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QuestionProvider>
      <Component {...pageProps} />
    </QuestionProvider>
  );
}

export default MyApp;
