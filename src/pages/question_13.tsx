// src/app/questions/Question1.tsx

import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Question from '@/app/components/Question';
import "@/app/globals.css";
import { useQuestionContext } from '@/contexts/QuestionContext';

const Question1: React.FC = () => {
  const router = useRouter();
  const [value, setValue] = useState<number>(0);

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 13, answer: value });
    router.push(route)
  }

  useEffect(() => {
    const found = answers.find((it) => it.questionId === 13);
    if (found) {
      setValue(found.answer);
    }
  }, [answers]);


  return (
    <Question
      title="Questão 13"
      // question="Em uma escala de 0 a 10, como você avalia sua habilidade em liderar equipes de diferentes tamanhos e compostas por pessoas diversas?"
      question="On a scale from 0 to 10, how would the candidate rate their ability to lead teams of various sizes and diverse individuals?"
      onPrevious={() => onGoTo("/question_12")}
      onNext={() => onGoTo("/question_14")}
    >
      <input
        type="range"
        min={0}
        max={10}
        value={value}
        onChange={(e) => setValue(Number(e.target.value))}
        className="w-full h-1 mb-6 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm dark:bg-gray-500"
      />
      <div className="flex justify-between">
        <p className="text-xl font-light mb-4 text-gray-700 text-justify">
          0 point
        </p>
        <p className="text-xl font-light mb-4 text-gray-700 text-justify">
          10 points
        </p>
      </div>
      <p className="text-xl font-light mb-4 text-gray-700 text-justify">
        Response: {value} point(s)
      </p>
    </Question>
  );
};

export default Question1;
