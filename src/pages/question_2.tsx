// src/app/questions/Question1.tsx

import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import "@/app/globals.css";
import { useQuestionContext } from "@/contexts/QuestionContext";

const Question1: React.FC = () => {
  const router = useRouter();
  const [option, setOption] = useState<string>("No");

  const handleOptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setOption(e.target.value);
  };

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 2, answer: option === "Yes" ? 1 : 0 });
    router.push(route);
  };

  useEffect(() => {
    const found = answers.find((it) => it.questionId === 2);
    if (found) {
      setOption(found!.answer === 1 ? "Yes" : "No");
    }
  }, [answers]);

  return (
    <Question
      title="Question 2"
      question="In your evaluation, has the candidate obtained a university degree or completed a recognized graduation program at an accredited educational institution?"
      onPrevious={() => onGoTo("/question_1")}
      onNext={() => onGoTo("/question_3")}
    >
      <div className="p-2 w-full text-black">
        <div className="flex flex-row w-full text-black">
          <label className="inline-flex items-center">
            <input
              className="text-gray-700 m-2"
              type="radio"
              value="No"
              checked={option === "No"}
              onChange={handleOptionChange}
            />
            <span className="text-base font-light text-gray-700">No</span>
          </label>
          <label className="inline-flex items-center">
            <input
              className="text-gray-700 m-2 "
              type="radio"
              value="Yes"
              checked={option === "Yes"}
              onChange={handleOptionChange}
            />
            <span className="text-base font-light text-gray-700">Yes</span>
          </label>
        </div>
      </div>
    </Question>
  );
};

export default Question1;
