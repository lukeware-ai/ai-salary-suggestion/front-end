// src/app/questions/Question1.tsx

import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Question from "@/app/components/Question";
import "@/app/globals.css";
import { useQuestionContext } from "@/contexts/QuestionContext";

const Question1: React.FC = () => {
  const router = useRouter();

  const { answers, setAnswer } = useQuestionContext();
  const onGoTo = (route: string) => {
    setAnswer({ questionId: 14, answer: option === "PJ" ? 1 : 0 });
    router.push(route);
  };

  const [option, setOption] = useState<string>("CLT");

  const handleOptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setOption(e.target.value);
  };

  useEffect(() => {
    const found = answers.find((it) => it.questionId === 14);
    if (found) {
      setOption(found!.answer === 1 ? "PJ" : "CLT");
    }
  }, [answers]);

  return (
    <Question
      title="Questão 14"
      // question="O candidato será contratado como PJ (Pessoa Jurídica) ou CLT (Consolidação das Leis do Trabalho)?"
      question="May I inquire whether the candidate will be hired as a PJ (Pessoa Jurídica) contractor or under the CLT (Consolidação das Leis do Trabalho) employment?"
      onPrevious={() => onGoTo("/question_13")}
      onNext={() => onGoTo("/finished")}
    >
      <div className="p-2 w-full text-black">
        <div className="flex flex-row w-full text-black">
          <label className="inline-flex items-center">
            <input
              className="text-gray-700 m-2"
              type="radio"
              value="CLT"
              checked={option === "CLT"}
              onChange={handleOptionChange}
            />
            <span className="text-xl font-light text-gray-700">CLT</span>
          </label>
          <label className="inline-flex items-center">
            <input
              className="text-gray-700 m-2 "
              type="radio"
              value="PJ"
              checked={option === "PJ"}
              onChange={handleOptionChange}
            />
            <span className="text-xl font-light text-gray-700">PJ</span>
          </label>
        </div>
      </div>
    </Question>
  );
};

export default Question1;
