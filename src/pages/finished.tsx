import "@/app/globals.css";
import { useQuestionContext } from "@/contexts/QuestionContext";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { FcHighPriority, FcOk } from "react-icons/fc";
import { GoHomeFill } from "react-icons/go";

const Question1: React.FC = () => {
  const router = useRouter();

  const { answers, clearAnswers } = useQuestionContext();
  const [value, setValue] = useState<string>();
  const [status, setStatus] = useState<boolean>(false);

  const formatCurrency = (
    value: number,
    locale = "pt-BR",
    currency = "BRL"
  ) => {
    return new Intl.NumberFormat(locale, {
      style: "currency",
      currency: currency,
    }).format(value);
  };

  useEffect(() => {
    const checkHealth = async () => {
      try {
        const response = await fetch("/api/check_health");
        const data = await response.json();
        setStatus(data.status === "success");
      } catch (error) {
        setStatus(false);
      }
    };

    checkHealth();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("/api/predict", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            tempo_experiencia: answers[0].answer,
            faculdade_concluida_0_1: answers[1].answer,
            pos_graduado_0_1: answers[2].answer,
            conhecimento_programacao_0_10: answers[3].answer,
            conheciemento_das_ferramentas_trabalho_0_10: answers[4].answer,
            conhecimento_banco_de_dados_0_10: answers[5].answer,
            conhecimento_back_end_0_10: answers[6].answer,
            conhecimento_front_end_0_10: answers[7].answer,
            conhecimento_devops_0_10: answers[8].answer,
            communicacao_0_10: answers[9].answer,
            dominio_sistemas_operacionais_0_5: answers[10].answer,
            conhecimento_basico_rede_0_5: answers[11].answer,
            lideranca_0_10: answers[12].answer,
            pj_0_1: answers[13].answer,
          }),
        });

        if (!response.ok) {
          throw new Error("Failed to fetch prediction");
        }

        const data = await response.json();
        setValue(formatCurrency(data.data.label));
      } catch (error: any) {
        console.error(error);
      }
    };

    fetchData();
  }, [answers]);

  return (
    <>
      <div className="relative">
        <div className="absolute top-0 left-0 m-4">
          <GoHomeFill
            className="w-8 h-8 text-gray-700"
            onClick={() => router.push("/")}
          />
        </div>
      </div>
      <div className="relative">
        <div className="absolute top-0 right-0 m-4">
          {status === true && <FcOk className="w-5 h-5 text-gray-700" />}
          {status === false && (
            <FcHighPriority className="w-5 h-5 text-gray-700" />
          )}
        </div>
      </div>
      <div className="flex flex-col items-center justify-center min-h-screen p-4 bg-gray-100">
        <div className="bg-white p-6 rounded-lg shadow-lg w-full max-w-md">
          <h1 className="text-[17px] sm:text-2xl font-bold mb-4 text-gray-700 uppercase">
            CONGRATULATIONS, WE'VE REACHED THE END!
          </h1>
          {/* <h2 className="text-xl font-light mb-4 text-gray-700 text-justify">Com base nas respostas, o salário sugerido a ser proposto ao candidato é de:</h2> */}
          <h2 className="text-base sm:text-xl font-light mb-4 text-gray-700 text-justify">
            Based on the answers provided, the suggested salary proposal for the
            candidate is:
          </h2>
          <div className="mb-4 text-purple-700 font-medium text-3xl text-center">
            {value}
          </div>
          <div className="flex flex-col md:flex-row justify-between">
            <button
              className="bg-gray-300 text-white py-2 px-4 md:px-10 rounded hover:bg-gray-400 mb-2 md:mb-0 md:mr-2"
              onClick={() => {
                router.push("/question_14");
              }}
            >
              Back
            </button>
            <button
              className="bg-fuchsia-500 text-white py-2 px-4 md:px-10 rounded hover:bg-fuchsia-700 mb-2 md:mb-0 md:mx-2"
              onClick={() => router.push("/correct")}
            >
              Correct
            </button>
            <button
              className="bg-purple-600 text-white py-2 px-4 md:px-10 rounded hover:bg-purple-700"
              onClick={() => {
                clearAnswers();
                router.push("/");
              }}
            >
              Finish
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Question1;
