// contexts/QuestionContext.tsx
import React, {
  createContext,
  useContext,
  useState,
  ReactNode,
  useEffect,
} from "react";

type Answer = {
  questionId: number;
  answer: number;
};

type BudgetContextType = {
  answers: Answer[];
  setAnswer: (answer: Answer) => void;
  clearAnswers: () => void;
};

const QuestionContext = createContext<BudgetContextType | undefined>(undefined);

export const useQuestionContext = () => {
  const context = useContext(QuestionContext);
  if (!context) {
    throw new Error(
      "useQuestionContext must be used within a QuestionProvider"
    );
  }
  return context;
};

type QuestionProviderProps = {
  children: ReactNode;
};

export const QuestionProvider: React.FC<QuestionProviderProps> = ({
  children,
}) => {
  const [answers, setAnswers] = useState<Answer[]>([]);

  // Load answers from localStorage when component mounts
  useEffect(() => {
    const storedAnswers = localStorage.getItem("answers");
    if (storedAnswers) {
      setAnswers(JSON.parse(storedAnswers));
    }
  }, []);

  const setAnswer = (answer: Answer) => {
    setAnswers((prevAnswers) => {
      const newAnswers = [
        ...prevAnswers.filter((a) => a.questionId !== answer.questionId),
        answer,
      ];
      localStorage.setItem("answers", JSON.stringify(newAnswers)); // Save to localStorage
      return newAnswers;
    });
  };

  const clearAnswers = () => {
    setAnswers([]);
    localStorage.removeItem("answers");
  };

  return (
    <QuestionContext.Provider value={{ answers, setAnswer, clearAnswers }}>
      {children}
    </QuestionContext.Provider>
  );
};
